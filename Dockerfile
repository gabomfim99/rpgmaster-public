# Use the official image as a parent image.
FROM node:current-slim

# Set the working directory.
WORKDIR /code

# Copy the rest of your app's source code from your host to your image filesystem.
COPY ./display .

#install node
RUN apt-get update && apt-get install -y curl
RUN curl -sL https://deb.nodesource.com/setup_8.x | bash -
RUN apt-get update && apt-get install -y nodejs

# Copy the file from your host to your current location.
#COPY package.json /code/package.json

# Run the command inside your image filesystem.
RUN npm install

# Inform Docker that the container is listening on the specified port at runtime.
EXPOSE 8080

# Run the specified command within the container.
CMD [ "npm", "run", "dev-start" ]

CMD [ "npm", "run", "test" ]