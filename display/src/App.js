import React from 'react';
import Application from './components/Application.js'
import 'bootstrap/dist/css/bootstrap.min.css'

function App() {
    return (
        <div id="main">
            <Application />
        </div>
    );
}

export default App;
