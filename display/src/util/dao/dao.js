//Implementamos facade na DAO para esconder a complexidade das operações de acesso ao banco. basicamente um CRUD

const dbPath = './src/util/dao/data/RPGMasterDB.db';

const convertValueToSQLValue = value =>
  typeof value === "string"
    ? `'${value}'`
    : value;

export function dbInsert(tableName, columns, values) {
    return new Promise((resolve, reject) => {
      let SQL = window.require('sqlite3').verbose();
      const db = new SQL.Database(dbPath);

      db.serialize(function(){
        let query = " INSERT INTO " + tableName + " " + columns + " VALUES " + values;

        console.log("query inserida: " + query);

        db.run(query, function(err) {
          if (err) {
            reject(err);
          } else {
            resolve("inserido com sucesso com a query: " + query);
          }
        });

        db.close();
      });
    });
  }

export function dbInsertObject(tableName, object) {
    let columns = [];
    let values = [];

    //extrai os atributos do objeto classe e coloca eles no array attributes
    columns = Object.keys(object);

    //Adiciona os valores dos atributos no array values
    values = Object.values(object);

    let columnsText = ` (${columns.join(',')}) `;

    values = values.map(function (value) {
        if (typeof value === "string"){
            return `'${value}'`;
        }
        else{
            return value;
        }
    });


    let valuesText = ` (${values.join(',')}) `;

    dbInsert(tableName,columnsText, valuesText)
        .then(value => console.log("valores inseridos:", value))
        .catch(err => console.log("não inserido", err));
}

export function dbSelectAndOperate(columnLabels, tableName, condition, onComplete) {
    let SQL = window.require('sqlite3').verbose();
    let db = new SQL.Database(dbPath);
    let sql = " SELECT " + columnLabels + " FROM " + tableName;
    sql = (condition == null || condition === "") ? sql : sql + " WHERE " + condition;
    let result = [];
    db.each(sql, (err, row) => {
        result.push(row)
    }, () => onComplete(result));
    console.log(result);
    db.close();
    return result;
}

export function dbSelect(columnLabels, tableName, condition) {
    let SQL = window.require('sqlite3').verbose();
    const db = new SQL.Database(dbPath);
    let sql = " SELECT " + columnLabels + " FROM " + tableName;
    sql = (condition == null || condition === "") ? sql : sql + " WHERE " + condition;
    console.log("A query é ", sql);
    let result = [];
    db.each(sql, (err, row) => {
        result.push(row);
    });
    db.close();
    return result;
}



export function dbUpdateObject(tableName, object, condition){
    const assignments = Object.entries(object) 
    .map(([key, value]) => `${key}=${convertValueToSQLValue(value)}`) 
    .join(", "); 
    dbUpdate(tableName, assignments, condition);
}

export function dbUpdate(tableName, assignments, condition) {
    let SQL = window.require('sqlite3').verbose();
    let db = new SQL.Database(dbPath);

    let sql = ' UPDATE ' + tableName +
                ' SET ' + assignments +
                ' WHERE ' + condition;

    db.run(sql);
    db.close();
}

export function dbDelete(targetId, tableName) {

}
