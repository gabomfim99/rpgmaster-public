//para importar seguir essa sintaxe
//import Classe from './Classe.js'
import Classe from './Classe'
import Race from './Classe'
import Attribute from './Atributte'

//Implementamos facade em Classe para aplicar as regras de negocio relacionadas antes de enviar ao banco

import {dbInsertObject, dbUpdateObject, dbSelect} from '../dao/dao';

export default class Char{

    static tabela = "CHARACTER";

    constructor(charName, playerName, classe, race, level, sexo, atributos, hp_max, mp_max){
        this.setCharName(charName);
        this.setPlayerName(playerName);
        this.Char_class_id = classe;
        this.Char_race_id = race;
        this.setLevel(level);
        this.Char_gender = sexo;
        this.Char_strength = atributos.strenght;
        this.Char_dexterity = atributos.dexterity;
        this.Char_constitution = atributos.constitution;
        this.Char_intelligence = atributos.intelligence;
        this.Char_wisdom = atributos.wisdom;
        this.Char_charisma = atributos.charisma;
        this.Char_max_hp = hp_max;
        this.Char_current_hp = hp_max;
        this.Char_max_mp = mp_max;
        this.Char_current_mp = mp_max;
    }



    static insert = (char) => {
        dbInsertObject(this.tabela, char);

    }

    static update = (char, condition) => {
        dbUpdateObject(this.tabela, char, condition);
    }

    static select = (columnLabelsArray, condition) => { // só funciona com arrays, mesmo se quisermos apenas uma tabela. Isso é meio estranho.
        let columnLabelsText = columnLabelsArray.join(',');
        let resultArray = dbSelect(columnLabelsText, this.tabela, condition);
        return resultArray;
    }

    setCharName(value){
        if ((typeof value) == "string"){
            if(value.length <= 20 && value.length > 0){
                this.Char_name = value;
            }
            else{
                console.log("[IGNORE IN TESTS] Error: Number of characteres in String is out of the expected range");
            }

        }else{
            console.log("[IGNORE IN TESTS] Error: Tried to insert wrong type in object attribute");
        }
    }

    setPlayerName(value){
        if ((typeof value) == "string"){
            if(value.length <= 20 && value.length > 0){
                this.Char_player_name = value;
            }
            else{
                console.log("[IGNORE IN TESTS] Error: Number of characteres in String is out of the expected range");
            }

        }else{
            console.log("[IGNORE IN TESTS] Error: Tried to insert wrong type in object attribute");
        }
    }

    setLevel(value){
        if ((typeof value) == "number"){
            if(value <= 20 && value > 0){
                this.Char_level = value;
            }
            else{
                console.log("[IGNORE IN TESTS] Error: Number of characteres in String is out of the expected range");
            }

        }else{
            console.log("[IGNORE IN TESTS] Error: Tried to insert wrong type in object attribute");
        }
    }

}
