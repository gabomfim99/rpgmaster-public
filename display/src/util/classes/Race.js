//para importar seguir essa sintaxe
//import Race from './Race.js'

//Implementamos facade em Race para aplicar as regras de negocio relacionadas antes de enviar ao banco

import {dbInsertObject, dbUpdateObject, dbSelect} from '../dao/dao';

export default class Race{

    static tabela = "RACE";

    constructor(name, id){
        this.Race_id = id;
        this.Race_name = name;
    }

    static insert = (race) => {
        dbInsertObject(this.tabela, race);

    }

    static update = (race, condition) => {
        dbUpdateObject(this.tabela, race, condition);
    }

    static select = (columnLabelsArray, condition) => { // só funciona com arrays, mesmo se quisermos apenas uma tabela. Isso é meio estranho.
        let columnLabelsText = columnLabelsArray.join(',');
        let resultArray = dbSelect(columnLabelsText, this.tabela, condition);
        return resultArray;
    }


}
