//para importar seguir essa sintaxe
//import Classe from './Classe.js'

//Implementamos facade em Classe para aplicar as regras de negocio relacionadas antes de enviar ao banco

import {dbInsertObject, dbUpdateObject, dbSelect} from '../dao/dao';

export default class Classe{

    static tabela = "CLASS";

    constructor(name, code, description){
        this.Class_name = name;
        this.Class_id = code;
        this.Class_description = description;
    }

    static insert = (classe) => {
        dbInsertObject(this.tabela, classe);

    }

    static update = (classe, condition) => {
        dbUpdateObject(this.tabela, classe, condition);
    }

    static select = (columnLabelsArray, condition) => { // só funciona com arrays, mesmo se quisermos apenas uma tabela. Isso é meio estranho.
        let columnLabelsText = columnLabelsArray.join(',');
        let resultArray = dbSelect(columnLabelsText, this.tabela, condition);
        return resultArray;
    }

    //precisamos retornar o select dessa maneira? se sim temos que usar promisses????
    /*return(
        <Table bordered hover>
            <thead>
                <tr>
                    <th>Personagem</th>
                    <th>Jogador</th>
                    <th>Nível</th>
                    <th>HP</th>
                    <th>Raça</th>
                    <th>Classe</th>
                </tr>
            </thead>
            <SheetRows cList={props.charList}/>
        </Table>
    );*/

    /*static selectAll = () => {
        dbSelect("*", this.tabela, "", function(){ return 1;});
    }*/

    /*metodo = () => {
        console.log("MAOI");
    }*/

}
