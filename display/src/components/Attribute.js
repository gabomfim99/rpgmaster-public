import React from 'react';

export default class Attribute extends React.Component {
    constructor() {
        super();
        this.state = {
            attrLevel: 0,
            attrMod: -5,
        };
        this.levelUp = this.levelUp.bind(this);
        this.levelDown = this.levelDown.bind(this);
        this.setAttrMod = this.setAttrMod.bind(this);
    }

    levelUp() {
        this.setState({
            attrLevel: this.state.attrLevel + 1,
        });
        this.setAttrMod(this.state.attrLevel + 1);
    }

    levelDown() {
        if(this.state.attrLevel - 1 >= 0){
            this.setState({
                attrLevel: this.state.attrLevel - 1,
            });
            this.setAttrMod(this.state.attrLevel - 1);
        }
    }

    setAttrMod(currentLevel) {
        if(currentLevel % 2 === 0){ //Se o nível de habilidade é par...
            this.setState({
                attrMod: (currentLevel / 2) - 5
            });
        }else if(currentLevel % 2 === 0){ //Se o nível de habilidade é ímpar...
            this.setState({
                attrMod: ((currentLevel - 1) / 2) - 5
            });
        }
    }

    handleChange(event){
        if(event.target.value >= 0){
            this.setState({
                attrLevel: event.target.value
            });
        }
    }

    render(){
        return(
            <div>
                <h2>{this.props.attrName}</h2>
                <h1>
                    {this.state.attrLevel}{this.props.attrAdjust >= 0? '+'+this.props.attrAdjust : this.props.attrAdjust}
                    <button onClick={this.levelUp}>+</button>
                    <button onClick={this.levelDown}>-</button>
                </h1>
                <h3>Modificador: ({this.state.attrMod >= 0? '+'+this.state.attrMod : this.state.attrMod})</h3>
            </div>
        );
    }
}
