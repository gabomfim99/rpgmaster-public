import React from "react"
import { MDBCard, MDBRow, MDBCol, MDBCardHeader, MDBCardBody} from 'mdbreact'

function Battle() {
    return (
        <MDBRow>
            <MDBCol lg="2"></MDBCol>
            <MDBCol lg="8">
                <MDBCard color="elegant-color" text="white">
                    <MDBCardHeader>
                        <h1>Batalhas em andamento: </h1>
                    </MDBCardHeader>

                    <MDBCardBody>
                        <p>Não existem batalhas em andamento</p>
                    </MDBCardBody>
                </MDBCard>
            </MDBCol>
            <MDBCol lg="2"></MDBCol>
        </MDBRow>
    );
}
export default Battle