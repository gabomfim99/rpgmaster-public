import React from 'react';

import '../css/mastertab/style.css';
import {Navbar, Nav} from 'react-bootstrap'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome/index.js'
import { faFire, faAddressBook, faArchive, faPastafarianism, faUsers, faTools } from '@fortawesome/free-solid-svg-icons'

//Tabs e seus IDs correspondentes
const HOME = 0
const BATTLE = 1 //Batalha: mostrar a tela de batalha
const SHEETS = 2 //Fichas: mostrar a tela de fichas de jogadores, com o mestre podendo ver a ficha de cada um ou criar uma nova para um novo jogador.
const ITEMS = 3 //Itens: mostrar a tela com todos os itens do jogo e suas informações; o mestre também pode criar novos itens.
const MONSTERS = 4 //Monstros: mostrar a tela com todos os monstros do jogo e suas informações; o mestre também pode criar novos monstros.
const NPCS = 5 //NPCs: mostrar a tela com todos os NPCs do jogo e suas informações; o mestre também pode criar novos NPCs.
const CONFIG = 6//Configurações: mostrar a tela de ajustes do aplicativo.


export default class MasterTab extends React.Component {
    getTabName(){
        const tabCode = this.props.main.state.curTabCode;
        if(tabCode === HOME){
            return(
                "RPGMaster"
            );
        }else if(tabCode === BATTLE){
            return(
                "Batalha"
            );
        }else if(tabCode === SHEETS){
            return(
                "Fichas de Jogador"
            );
        }else if(tabCode === ITEMS){
            return(
                "Itens"
            );
        }else if(tabCode === MONSTERS){
            return(
                "Monstros"
            );
        }else if(tabCode === NPCS){
            return(
                "NPCs"
            );
        }else if(tabCode === CONFIG){
            return(
                "Configurações"
            );
        }
    }

    handleClick(tabCode){
        this.props.main.setState({
            curTabCode: tabCode
        });
    }

    render() {
        return (
            <div id="mainNavbar">
                <Navbar expand="lg" className="navbarBG">
                    <Navbar.Brand href="#" onClick={() => this.handleClick(HOME)}>
                        <img src={require("../assets/images/placeholder-logo.png")} alt="RPGMaster" className="rotate" style={{height: "6vw", width: "6vw"}}/>
                    </Navbar.Brand>
                    <Navbar.Toggle aria-controls="mainNavbarTabs" />
                    <Navbar.Collapse id="mainNavbarTabs" className="justify-content-end">
                        <Nav>
                            <Nav.Item><button className="navButton" onClick={() => this.handleClick(BATTLE)}><FontAwesomeIcon icon={faFire} size="2x"/><br/>Batalha</button></Nav.Item>
                            <Nav.Item><button className="navButton" onClick={() => this.handleClick(SHEETS)}><FontAwesomeIcon icon={faAddressBook} size="2x"/><br/>Fichas</button></Nav.Item>
                            <Nav.Item><button className="navButton" onClick={() => this.handleClick(ITEMS)}><FontAwesomeIcon icon={faArchive} size="2x"/><br/>Itens</button></Nav.Item>
                            <Nav.Item><button className="navButton" onClick={() => this.handleClick(MONSTERS)}><FontAwesomeIcon icon={faPastafarianism} size="2x"/><br/>Monstros</button></Nav.Item>
                            <Nav.Item><button className="navButton" onClick={() => this.handleClick(NPCS)}><FontAwesomeIcon icon={faUsers} size="2x"/><br/>NPCs</button></Nav.Item>
                            <Nav.Item><button className="navButton" onClick={() => this.handleClick(CONFIG)}><FontAwesomeIcon icon={faTools} size="2x"/><br/>Configurações</button></Nav.Item>
                        </Nav>
                    </Navbar.Collapse>
                </Navbar>
                <Navbar expand="lg" className="navFooter" style={{padding: "0 0 0 1vw"}}>
                    {this.getTabName()}
                </Navbar>
                <br/>
            </div>
        );
    }
}
