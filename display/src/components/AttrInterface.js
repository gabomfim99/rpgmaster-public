import React from 'react';

import Attribute from './Attribute.js'

export default class AttrInterface extends React.Component {
    render(){
        return(
            <div>
                <Attribute attrName="Força" attrAdjust={this.props.attrAdjust[0]}/>
                <Attribute attrName="Destreza" attrAdjust={this.props.attrAdjust[1]}/>
                <Attribute attrName="Constituição" attrAdjust={this.props.attrAdjust[2]}/>
                <Attribute attrName="Inteligência" attrAdjust={this.props.attrAdjust[3]}/>
                <Attribute attrName="Sabedoria" attrAdjust={this.props.attrAdjust[4]}/>
                <Attribute attrName="Carisma" attrAdjust={this.props.attrAdjust[5]}/>
            </div>
        );
    }
}
