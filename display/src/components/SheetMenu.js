import React from 'react';
import { useState } from "react";
import {Button} from 'react-bootstrap'
import {MDBCard, MDBRow, MDBCol} from 'mdbreact'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome/index.js'
import { faPlus, faFileImport, faSearch } from '@fortawesome/free-solid-svg-icons'
import '../css/playersheet/sheetsearch/style.css'
import '../../node_modules/mdbreact/dist/css/mdb.css';
import SheetMain from "./SheetMain"

function SheetMenu() {
    let [active, setText] = useState(0);
    return (
        <MDBRow> 
            <SheetMain 
                activeButton = {active} //botão que foi clicado
            /> 
            <MDBCol lg="2">
                <MDBCard color="elegant-color" text="white" className="text-center">


                    <Button onClick={() => setText(2)} variant="dark">
                        <FontAwesomeIcon icon={faPlus} size="3x"/>
                        <h4>Nova Ficha</h4>
                    </Button>


                    <br/>
                    <Button onClick={() => setText(1)} variant="dark">
                        <FontAwesomeIcon icon={faFileImport} size="3x"/>
                        <h4>Importar Ficha</h4>
                    </Button>


                    <br/>
                    <Button onClick={() => setText(0)} variant="dark">
                        <FontAwesomeIcon icon={faSearch} size="3x"/>
                        <h4>Todas as Fichas</h4>
                    </Button>
                </MDBCard>
            </MDBCol>
        </MDBRow>
    );
}

export default SheetMenu