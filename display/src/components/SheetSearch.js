import React from 'react';
import {MDBContainer} from 'mdbreact'
import '../css/playersheet/sheetsearch/style.css'
import '../../node_modules/mdbreact/dist/css/mdb.css';
import SheetMenu from "./SheetMenu"


export default class SheetSearch extends React.Component {
    constructor() {
        super();
        this.state = {
            charList: [],
        };
    }

    setCharList = (charList) => this.setState({charList: charList});

    render() {
        return(
            <div id="sheetsearch">
                <MDBContainer>
                    <SheetMenu />
                </MDBContainer>
            </div>
        );
    }
}
