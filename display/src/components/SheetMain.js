import React from 'react';
import Char from '../util/classes/Char';
import {Form, FormControl, Col, FormGroup, Dropdown} from 'react-bootstrap'
import {MDBCard, MDBCardHeader, MDBCardBody, MDBRow, MDBCol, MDBBtn} from 'mdbreact'
import '../css/playersheet/sheetsearch/style.css'
import '../../node_modules/mdbreact/dist/css/mdb.css';
import RPGMTable from '../components/RPGMTable';
import Atributte from '../util/classes/Atributte';
import Classe from '../util/classes/Classe';
import Race from '../util/classes/Race';
const DAO = require('../util/dao/dao.js');


function SheetMain(props) {

    function inserirFicha() {
        let strength = parseInt(document.getElementById('strength').value);
        let dexterity = parseInt(document.getElementById('dexterity').value);
        let constitution = parseInt(document.getElementById('constitution').value);
        let intelligence = parseInt(document.getElementById('intelligence').value);
        let wisdom = parseInt(document.getElementById('wisdom').value);
        let charisma = parseInt( document.getElementById('charisma').value);

        let atributte = new Atributte(strength,dexterity,constitution,intelligence,wisdom,charisma);

        let charName = document.getElementById('charName').value;

        let playerName = document.getElementById('playerName').value;
    
        //let charClass = Classe.select(["Class_id"], `Class_name = '${document.getElementById('charClass').value}'`); 
        //console.log("MINHA CLASSE EH: ", charClass.length, charClass);
        let charClass = document.getElementById('charClass').value;

        //let charRace = Race.select(["Race_id"], `Race_name = '${document.getElementById('charRace').value}'`); 
        //console.log( charRace);
        let charRace = document.getElementById('charRace').value;


        let charLevel = parseInt(document.getElementById('charLevel').value);

        let charGenre
        if(document.getElementById('charGenre').value === 'Homem'){
            charGenre = 1;
        }else{
            charGenre = 0;
        }


        let max_hp = parseInt(document.getElementById('max_hp').value);
        let max_mp = parseInt(document.getElementById('max_mp').value);


        //console.log(atributte, charName,playerName,charClass,charRace,charLevel,charGenre,max_hp,max_mp);
        
        let character = new Char(charName, playerName, charClass, charRace, charLevel, charGenre, atributte, max_hp, max_mp);
        Char.insert(character);

        document.getElementById('userFeedback').innerHTML = "Inserido com sucesso";
    }

    if (props.activeButton == 0) {
        return (
            <MDBCol lg="10">
            <MDBCard color="elegant-color" text="white">
                <MDBCardHeader>
                    <MDBRow>
                        <MDBCol lg="6">
                            <h2>Suas Fichas</h2>
                        </MDBCol>
                        <MDBCol lg="6">
                            <Form>
                                <Form.Row>
                                    <Col>
                                        <FormControl type="text" placeholder="Buscar por nome de jogador ou personagem..."/>
                                    </Col>
                                </Form.Row>
                            </Form>
                        </MDBCol>
                    </MDBRow>
                </MDBCardHeader>
                <MDBCardBody>
                    <MDBCard>
                        <RPGMTable inputClass={Char} columnLabelsArray={['Char_name as Personagem','Char_player_name as Jogador']} condition=""/>
                    </MDBCard>
                </MDBCardBody>
            </MDBCard>
            </MDBCol>
        );   
    } else if (props.activeButton === 2){
        return (
            <MDBCol lg="10">
            <MDBCard color="elegant-color" text="white">
                <MDBCardHeader>
                    <MDBRow>
                        <MDBCol lg="12">
                            <h2>Inserir Nova Ficha</h2>
                        </MDBCol>
                    </MDBRow>
                </MDBCardHeader>
                <MDBCardBody>
                    <Form>
                        <Form.Row>
                            <Col>
                                <label>Nome do novo personagem</label><FormControl id="charName" type="text" placeholder="Digite o nome do novo personagem..."/>
                            </Col>
                        </Form.Row>
                        <br/>
                        <Form.Row>
                            <Col lg="5">
                                <label>Nome do novo jogador</label><FormControl id="playerName" type="text" placeholder="Digite o nome do novo jogador..."/>
                            </Col>
                            <Col lg="2"></Col>
                            <Col lg="5">
                                <label>Level</label><FormControl id="charLevel" type="text" placeholder="Digite o level do novo personagem..."/>
                            </Col>
                            
                        </Form.Row>
                        <br/>
                        <Form.Row>
                            <Col lg="5">
                                <Form.Group>
                                    <Form.Label>Classe</Form.Label>
                                    <Form.Control as="select"  id="charClass" type="text" placeholder="Digite a classe do novo personagem...">
                                        <option>Bbr</option>
                                        <option>Brd</option>
                                        <option>Clr</option>
                                        <option>Drd</option>
                                        <option>Fet</option>
                                        <option>Gue</option>
                                        <option>Lad</option>
                                        <option>Mag</option>
                                        <option>Mng</option>
                                        <option>Pal</option>
                                        <option>Rgr</option>
                                        <option>Smr</option>
                                        <option>Swb</option>
                                    </Form.Control>
                                </Form.Group>
                            </Col>
                            <Col lg="2"></Col>
                            <Col lg="5">
                                <Form.Group>
                                    <Form.Label>Raça</Form.Label>
                                    <Form.Control as="select"  id="charRace" type="text" placeholder="Digite a raça do novo personagem...">
                                        <option>An</option>
                                        <option>Elf</option>
                                        <option>Gob</option>
                                        <option>Half</option>
                                        <option>Hum</option>
                                        <option>Lef</option>
                                        <option>Mnt</option>
                                        <option>Qren</option>
                                    </Form.Control>
                                </Form.Group>
                            </Col>
                        </Form.Row>
                        <br/>
                        <Form.Row>
                            <Col lg="5">
                            <Form.Group>
                                <Form.Label>Gênero</Form.Label>
                                    <Form.Control as="select"  id="charGenre" type="text" placeholder="Digite o gênero do novo personagem...">
                                        <option>Homem</option>
                                        <option>Mulher</option>
                                    </Form.Control>
                                </Form.Group>
                            </Col>
                            <Col lg="2"></Col>
                            <Col lg="2">
                                <label>Força</label><FormControl id="strength" type="text" placeholder="Digite a força do novo personagem..."/>
                            </Col>
                            <Col lg="1"></Col>
                            <Col lg="2">
                                <label>Destreza</label><FormControl id="dexterity" type="text" placeholder="Digite a destreza do novo personagem..."/>
                            </Col>
                        </Form.Row>
                        <br/>
                        <Form.Row>
                            <Col lg="5">
                                <label>HP máximo</label><FormControl id="max_hp" type="text" placeholder="Digite o HP máximo do novo personagem..."/>
                            </Col>

                            <Col lg="2"></Col>
                            
                            <Col lg="2">
                                <label>Sabedoria</label><FormControl id="wisdom" type="text" placeholder="Digite a sabedoria do novo personagem..."/>
                            </Col>

                            <Col lg="1"></Col>

                            <Col lg="2">
                                <label>Carisma</label><FormControl id="charisma" type="text" placeholder="Digite a carisma do novo personagem..."/>
                            </Col>
                            
                            
                        </Form.Row>
                        <br/>
                        <Form.Row>
                             <Col lg="5">
                                <label>MP máximo</label><FormControl id="max_mp" type="text" placeholder="Digite o MP máximo do novo personagem..."/>
                            </Col>
                            <Col lg="2"></Col>

                            <Col lg="2">
                                <label>Constituição</label><FormControl id="constitution" type="text" placeholder="Digite a constituição do novo personagem..."/>
                            </Col>

                            <Col lg="1"></Col>

                            <Col lg="2">
                                <label>Inteligência</label><FormControl id="intelligence" type="text" placeholder="Digite a inteligência do novo personagem..."/>
                            </Col>
                            
                        </Form.Row>
                        <br/>
                        <MDBRow>
                            <Col lg="4"></Col>
                            <Col lg="4">
                                <p id="userFeedback" style={{textAlign: "center"}}></p>
                            </Col>
                            <Col lg="4"></Col>
                        </MDBRow>
                        
                        <Form.Row>
                            <Col lg="5"></Col>
                            <Col lg="2">
                                <MDBBtn onClick={inserirFicha.bind(this)} >Inserir Ficha</MDBBtn>
                            </Col>
                            <Col lg="5"></Col>
                        </Form.Row>
                    </Form>
                </MDBCardBody>
            </MDBCard>
            </MDBCol>
        );   
    } else {
        return (
            <MDBCol lg="10">
                <MDBCard color="elegant-color" text="white">
                    <MDBCardHeader>
                        <h1>Função não-implementada</h1>
                    </MDBCardHeader>
                    <MDBCardBody>
                        <h3>Não é possível importar uma ficha ainda.</h3>
                    </MDBCardBody>
                </MDBCard>
            </MDBCol>
        );
    }
}

export default SheetMain