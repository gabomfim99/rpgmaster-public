import React from "react"
import { useState } from "react";
import BodyContent from "./BodyContent"
import { MDBCard, MDBRow, MDBCol, MDBCardHeader, MDBCardBody, MDBBtn, MDBCardFooter } from 'mdbreact'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome/index.js'
import { faAngleRight } from "@fortawesome/free-solid-svg-icons"

function Home() {
    let [text, setText] = useState(0);
    return (
        <MDBRow>
            <MDBCol lg="2"></MDBCol>
            <MDBCol lg="8">
                <MDBCard color="elegant-color" text="white">
                    <MDBCardHeader>
                        <h1>Bem-vinda(o) ao RPGMaster</h1>
                    </MDBCardHeader>

                    <MDBCardBody>
                        <BodyContent 
                            txt={
                                text
                            }
                        />
                    </MDBCardBody>


                    <MDBCardFooter style={{display: text != 1 ? "block": "none"}}>
                        <MDBBtn onClick={() => setText(++text)}><h5>Continuar <FontAwesomeIcon icon={faAngleRight}/></h5></MDBBtn>
                    </MDBCardFooter>
                </MDBCard>
            </MDBCol>
            <MDBCol lg="2"></MDBCol>
        </MDBRow>
    );
}

export default Home