import React, {useEffect, useState} from 'react';

import {Table} from 'react-bootstrap'
import Char from '../util/classes/Char';

function RPGMTableHeader(props){
    const cols = props.columnLabelsArray.map(
        col => {
            if(col.toLowerCase().includes(" as ")){
                return <td>{col.split(" ")[2]}</td>
            }else{
                return <td>{col}</td>
            }
        }
    );
    return(
        <thead>
            <tr>
                {cols}
            </tr>
        </thead>
    );
}

function RPGMTableRows(props){


    const [isUpdated, setUpdated] = useState(props.isUpdated);
    useEffect(() => {
        let rows = createJSX();
        setUpdated(true);

      }, [isUpdated]);

    function createJSX(){
        let rows = props.rows;
        console.log("RPGMTableRows", rows);
        if(rows.length > 0){
            rows = rows.map(objectInArray => {
                let fields = Object.values(objectInArray);
                //console.log("momento fodase:", fields);
                fields = fields.map(element => {
                    return <td>{element}</td>;
                });
                //console.log("valor do array:", fields);
                return fields;

            });

            console.log("O que ta aqui: ", rows);
            // rows = [[<td>Gabriel</td>,<td>21</td>],[<td>Eric</td>,<td>20</td>]]

            rows = rows.map(element => {
                //
                element = (<tr>
                            {element}
                        </tr>);
                //console.log("rows do ultimo array:", element)
                return element;
            });

            console.log("rows atualizados:", rows);
        }
        return rows
    }

    let rows = createJSX();


    return(
        <tbody>
            {rows}
        </tbody>
    );
}


//Props: inputClass; columnLabelsArray; condition;
export default class RPGMTable extends React.Component {
    constructor() {
        super();
        this.state = {
            rows: [],
            isUpdated: false
        };
    }

    componentDidMount(){
        //this.setState({rows: [{nome: "Pedrinho", idade: 21}, {nome: "Joaozinho", idade: 20}, {nome: "Fulaninho", idade: 20}]});
        let data = this.props.inputClass.select(this.props.columnLabelsArray, this.props.condition);

        this.setState({rows: data});

        console.log("as rows do DB", data);
    }

    render(){
        //<RPGMTableRows rows={this.state.rows}/>
        return(
            <Table bordered hover>
                <RPGMTableHeader columnLabelsArray={this.props.columnLabelsArray}/>
                <RPGMTableRows rows={this.state.rows} isUpdated={this.isUpdated}/>
            </Table>
        )
    }

    //this is the function responsible to update the state

}
