import React from 'react';

import MasterTab from './MasterTab.js'
import SheetSearch from './SheetSearch.js'
import Config from './Config'
import Home from './Home'
import Battle from './Battle'
import Itens from './Itens'
import Monstros from './Monstros'
import NPC from './NPC'

let sheetData = {
    columns: [
        {
            label: 'Personagem',
            field: 'char',
            sort: 'asc',
            width: 150,
        },
        {
            label: 'Jogador',
            field: 'player',
            sort: 'asc',
            width: 150,
        },
    ],
    rows: [
        {
            char: 'Jasper Woodfield',
            player: 'Gabriel Silveira',
        },
        {
            char: 'Mariadna Pygnoir',
            player: 'Julia123',
        },
    ]
}

function CurrentDisplay(props){ //Retorna o componente corresponde à aba atualmente selecionada.
    if(props.tabCode === 2){ //SHEETS
        return(
            <SheetSearch sheetData={sheetData} />
        );
    }else if(props.tabCode === 1){ //BATALHA
        return(
            <Battle />
        );
    }else if(props.tabCode === 3){ //ITENS
        return(
            <Itens />
        );
    }else if(props.tabCode === 4){ //Montros
        return(
            <Monstros />
        );
    }else if(props.tabCode === 5){ //NPC
        return(
            <NPC />
        );
    }else if(props.tabCode === 6){ //Config
        return(
            <Config />
        );
    }else{
        return(
            <Home />
        );
    }
}

export default class Application extends React.Component {
    constructor() {
        super();
        this.state = {
            curTabCode: 0 //Aba inicial: Home
        };
    }

    render() {
        return (
            <div>
                <MasterTab main={this} />
                <CurrentDisplay tabCode={this.state.curTabCode} />
            </div>
        );
    }
}
