import React from "react"

function BodyContent(props) {
    let text, subtext
    if (props.txt === 0) {
        text = "Nossa equipe desenvolveu esta aplicação para tornar épicas as suas incríveis aventuras jogando RPG!"
        subtext = "Vamos começar?"
    } else {
        text= "Este software é voltado para mestres com experiência. Nossas features ajudarão a controlar a partida para uma run justa e agradável para todos."
        subtext = ""   
        
    }
    return (
        <div>
            <p>
                {text} <br />
                {subtext}      
            </p>
            <p style={{display: props.txt === 1 ? "block": "none"}}>
                <h4>Batalha: </h4>
                <p>A aba Batalha mostra as batalhas em andamento no jogo para você não se perder nos valores.</p><br />
                <h4>Fichas: </h4>
                <p>A aba Fichas tem tudo sobre as fichas de quem já está jogando e também tem a possibilidade de adicionar novas fichas.</p><br />
                <h4>Itens: </h4>
                <p>A aba Itens mostra todos os itens do jogo e te possibilita adicionar novos itens também.</p><br />
                <h4>Monstros: </h4>
                <p>A aba Monstros te permite ver os montros no jogo e criar novos monstros de uma forma balanceada, i.é.: nenhum monstro apelão!</p><br />
                <h4>NPCs: </h4>
                <p>A aba NPCs mostra todos os NPCs do jogo</p><br />
                <h4>Configurações: </h4>
                <p>A aba Configurações te permite configurar o ambiente como você preferir. Quem sabe você não tenha mais inspiração vendo imagens de RPG? Isto é possível nesta aba.</p> <br />
                <h4>*Esta página inicial sempre estará disponível para te auxiliar em qualquer dúvida. Basta clicar no ícone girando no canto superior esquerdo que você volta aqui.</h4><br/><br/>
                <h3>Entendeu tudo? Então vá até a aba de fichas. Te desejamos uma boa partida!</h3>
            </p>
        </div>
    );
}
export default BodyContent