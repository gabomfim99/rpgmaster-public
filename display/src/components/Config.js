import React from 'react'
import {MDBCard, MDBCardHeader, MDBCardBody, MDBRow, MDBCol, MDBBtn} from 'mdbreact'

function Config() {
    return (
        <MDBRow>
        <MDBCol lg="2"></MDBCol>
        <MDBCol lg="8">
            <MDBCard color="elegant-color" text="white">
                <MDBCardHeader>
                    <h1>O que deseja configurar?</h1>
                </MDBCardHeader>

                <MDBCardBody>
                    
                    <MDBCol>
                        <h4>Ver imagens para inspiração: </h4>
                        <img src={require('../assets/images/steampunk.jpg')} width="500"/>
                    </MDBCol>
                    
                </MDBCardBody>

            </MDBCard>
        </MDBCol>
        <MDBCol lg="2"></MDBCol>
    </MDBRow>
    );
}
export default Config