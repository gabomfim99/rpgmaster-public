import React from "react"
import { MDBCard, MDBRow, MDBCol, MDBCardHeader, MDBCardBody} from 'mdbreact'

function Itens() {
    return (
        <MDBRow>
            <MDBCol lg="2"></MDBCol>
            <MDBCol lg="8">
                <MDBCard color="elegant-color" text="white">
                    <MDBCardHeader>
                        <h1>Itens no jogo: </h1>
                    </MDBCardHeader>

                    <MDBCardBody>
                        <p>Não existem itens no jogo</p>
                    </MDBCardBody>
                </MDBCard>
            </MDBCol>
            <MDBCol lg="2"></MDBCol>
        </MDBRow>
    );
}

export default Itens