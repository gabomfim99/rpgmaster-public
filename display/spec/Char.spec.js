import Char from '../src/util/classes/Char';

describe("charName", function() {
  let value;
  let character;

  beforeEach(function() {
    character = new Char("lorendar", "gabriel", 1, 1, 1, "m", [3,2,1,1,1,1], 100, 100, 10);
  });


    //Classes de equivalência

  it("should be equal a valid inserted setCharName argument value", function() {
    value = "Gabriel";
    character.setCharName(value);
    expect(character.charName).toEqual(value);
  });

  it("should not be equal a invalid inserted setCharName argument value", function() {
    value = "";
    character.setCharName(value);
    expect(character.charName).not.toEqual(value);
  });
  
  describe("type", function() {

    //Classes de equivalência

    it("that is not a string should fail to assign", function() {
      value = 5;
      character.setCharName(value);
      expect(typeof character.charName).toEqual("string");
    });

    it("should be a string", function() {
      value = "teste";
      character.setCharName(value);
      expect(typeof character.charName).toEqual("string");
    });
  
  });

  describe("number of characters", function() {

    //Classes de equivalência

    it("should not have less than one character", function() {
      value = "";
      character.setCharName(value);
      expect(character.charName.length).not.toBeLessThan(1);

    });

    it("should not have more than 20 characteres", function() {
      value = "012345678901234567891";
      character.setCharName(value);
      expect(character.charName.length).not.toBeGreaterThan(20);
    });

    it("should have a number of characters between 1 and 20", function() {
      value = "Gabriel";
      character.setCharName(value);
      expect(character.charName.length).toBeGreaterThanOrEqual(1);
      expect(character.charName.length).toBeLessThanOrEqual(20);
    });

    //Análise de valor limite
    
    it("charName with no characteres are not allowed", function() {
      let value = "";
      character.setCharName(value);
      expect(character.charName.length).not.toEqual(value.length);
    });

    it("charName with 21 characteres are not allowed", function() {
      let value = "012345678901234567891"; //21 caracteres
      character.setCharName(value);
      expect(character.charName.length).not.toEqual(value.length);
    });

    it("charNames with 1 character should be allowed to exist", function() {
      let value = "1";
      character.setCharName(value);
      expect(character.charName.length).toEqual(value.length);

    });

    it("charNames with 20 characteres should be allowed to exist", function() {
      let value = "01234567890123456789";//20 caracteres
      character.setCharName(value);
      expect(character.charName.length).toEqual(value.length);
    });

  });

});


//PlayerName

describe("playerName", function() {
  let value;
  let character;

  beforeEach(function() {
    character = new Char("lorendar", "default", 1, 1, 1, "m", [3,2,1,1,1,1], 100, 100, 10);
  });


    //Classes de equivalência

  it("should be equal a valid inserted setPlayerName argument value", function() {
    value = "Gabriel";
    character.setPlayerName(value);
    expect(character.playerName).toEqual(value);
  });

  it("should not be equal a invalid inserted setPlayerName argument value", function() {
    value = "";
    character.setPlayerName(value);
    expect(character.playerName).not.toEqual(value);
  });
  
  describe("type", function() {

    //Classes de equivalência

    it("that is not a string should fail to assign", function() {
      value = 5;
      character.setPlayerName(value);
      expect(typeof character.playerName).toEqual("string");
    });

    it("should be a string", function() {
      value = "teste";
      character.setPlayerName(value);
      expect(typeof character.playerName).toEqual("string");
    });
  
  });

  describe("number of characters", function() {

    //Classes de equivalência

    it("should not have less than one character", function() {
      value = "";
      character.setPlayerName(value);
      expect(character.playerName.length).not.toBeLessThan(1);

    });

    it("should not have more than 20 characteres", function() {
      value = "012345678901234567891";
      character.setPlayerName(value);
      expect(character.playerName.length).not.toBeGreaterThan(20);
    });

    it("should have a number of characters between 1 and 20", function() {
      value = "Gabriel";
      character.setPlayerName(value);
      expect(character.playerName.length).toBeGreaterThanOrEqual(1);
      expect(character.playerName.length).toBeLessThanOrEqual(20);
    });

    //Análise de valor limite
    
    it("playerName with no characteres are not allowed", function() {
      let value = "";
      character.setPlayerName(value);
      expect(character.playerName.length).not.toEqual(value.length);
    });

    it("playerName with 21 characteres are not allowed", function() {
      let value = "012345678901234567891"; //21 caracteres
      character.setPlayerName(value);
      expect(character.playerName.length).not.toEqual(value.length);
    });

    it("playerNames with 1 character should be allowed to exist", function() {
      let value = "1";
      character.setPlayerName(value);
      expect(character.playerName.length).toEqual(value.length);

    });

    it("playerNames with 20 characteres should be allowed to exist", function() {
      let value = "01234567890123456789";//20 caracteres
      character.setPlayerName(value);
      expect(character.playerName.length).toEqual(value.length);
    });

  });

});




//LEVEL

describe("level", function() {
  let value;
  let character;

  beforeEach(function() {
    character = new Char("lorendar", "gabriel", 1, 1, 1, "m", [3,2,1,1,1,1], 100, 100, 10);
  });

    //Classes de equivalência

  it("should be equal a valid inserted setLevel argument value", function() {
    value = 10;
    character.setLevel(value);
    expect(character.level).toEqual(value);
  });

  it("should not be equal a invalid inserted setLevel argument value", function() {
    value = "vinte";
    character.setLevel(value);
    expect(character.level).not.toEqual(value);
  });
  
  describe("type", function() {

    //Classes de equivalência

    it("that is not a number should fail to assign", function() {
      value = "Not a Number";
      character.setLevel(value);
      expect(typeof character.level).toEqual("number");
    });

    it("should be a number", function() {
      value = "teste";
      character.setLevel(value);
      expect(typeof character.level).toEqual("number");
    });
  
  });

  describe("number of characters", function() {

    //Classes de equivalência

    it("should not be less than 1", function() {
      value = 0;
      character.setLevel(value);
      expect(character.level).not.toBeLessThan(1);

    });

    it("should not be more than 20", function() {
      value = 21;
      character.setLevel(value);
      expect(character.setLevel).not.toBeGreaterThan(20);
    });

    it("should be between 1 and 20", function() {
      value = 10;
      character.setLevel(value);
      expect(character.level).toBeGreaterThanOrEqual(1);
      expect(character.level).toBeLessThanOrEqual(20);
    });

    //Análise de valor limite
    
    it("level 0 is not allowed", function() {
      let value = 0;
      character.setLevel(value);
      expect(character.level).not.toEqual(value);
    });

    it("level 21 is not allowed", function() {
      let value = 21;
      character.setLevel(value);
      expect(character.level).not.toEqual(value);
    });

    it("level 1 should be allowed to exist", function() {
      let value = 1;
      character.setLevel(value);
      expect(character.level).toEqual(value);

    });

    it("level 20 should be allowed to exist", function() {
      let value = 20;
      character.setLevel(value);
      expect(character.level).toEqual(value);
    });

  });


});
