function Pessoa() {
    //Dados Iniciais (mock)
    this.username = "default_username"
}

Pessoa.prototype.getUsername = function() {
    return this.username;
};

Pessoa.prototype.setUsername = function(username) {
    this.username = username;
};

module.exports = Pessoa;
