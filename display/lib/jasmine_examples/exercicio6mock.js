function Cliente() {
    //Dados Iniciais (mock)
    this.tipo = "A"
    this.capital = true
    this.valor = 0.0
    this.desconto = 0.0
}

Cliente.prototype.getTipo = function() {
    return this.tipo;
};

Cliente.prototype.setTipo = function(tipo) {
    this.tipo = tipo;
    this.updateMock();
};

Cliente.prototype.getCapital = function() {
    return this.capital;
};

Cliente.prototype.setCapital = function(capital) {
    this.capital = capital;
    this.updateMock();
};

Cliente.prototype.getValor = function() {
    return this.valor;
};

Cliente.prototype.setValor = function(valor) {
    this.valor = valor;
    this.updateMock();
};

Cliente.prototype.getDesconto = function() {
    return this.desconto;
};

Cliente.prototype.setDesconto = function(desconto) {
    this.desconto = desconto;
};

Cliente.prototype.updateMock = function () { //Simulação de mecanismo interno; dada uma mudança, a saída 'desconto' é atualizada conforme essa mudança (mock).
    if(this.tipo == "A"){
        this.setDesconto(0.15);
    }else if(this.getTipo() == "B"){
        if(this.getCapital()){
            this.setDesconto(0.10);
        }else{
            this.setDesconto(0.05);
        }
    }else if(this.getTipo() == "C"){
        if(this.getCapital()){
            if(this.getValor() >= 1000.00){
                this.setDesconto(0.10);
            }
        }else{
            this.setDesconto(0.00);
        }
    }
};

module.exports = Cliente;
