# RPG Master

Com nosso projeto, pretendemos auxiliar mestres de RPG de mesa à construírem jogos mais dinâmicos por meio de um produto digital, focado em desktops, que contenha as ferramentas necessárias para a implementação das regras do jogo e geração de conteúdo.

## Arquitetura

Utilizamos a arquitetura MVC para construir nossa aplicação. O nosso *Model* seria o banco de dados, nosso *View* seria a aplicação Desktop e nosso *Controller* seria o container de mesmo nome.

![Diagrama C4](img/arquiteturaRPG.png "Diagrama C4")

### Componentes

Essas estruturas pode ser utilizadas por diferentes componentes conforme a aplicação vá evoluindo. Como a aplicação está totalmente no lado do cliente, a comunicação entre componentes pode ser feita por meio de bibliotecas de funções providas pelos próprios componentes. 

**Cadastro de fichas de RPG:** cadastra as fichas dos jogadores, monstros ou NPCs. 

**Interface ao banco de dados:** lê e escreve no banco de dados de maneira segura e consistente. 

## Getting Started

Essas instruções vão permitir que você tenha uma cópia do projeto rodando no seu computador para propósitos de desenvolvimento e teste.

### Pré-requisitos

Nosso produto é um App desktop para multiplos sistemas operacionais, baseado em Electron.JS.
Para o desenvolvimento da aplicação, utilizamos também tecnologias como React.JS, bootstrap e SQLite3.
Para a instalação desses pacotes, é preciso utilizar o [NODE.js]( https://nodejs.org/en/download/). Estamos utilizando a versão LTS 12.16.3, que inclui npm 6.14.4.

### Instalação

Siga os seguintes passos:
Clone o repositório, navegue até a pasta do repositório, entre na pasta `display`, e instale os pacotes necessários para lançar a aplicação.

```
git clone https://gitlab.com/mc426-grupo3/rpgmaster.git
cd rpgmaster/display
npm install
```

Você pode tentar utilizar o comando `sudo npm install` caso tenha problemas na instação.

### Lançando a aplicação

Para lançar a aplicação, você deve executar algum dos seguintes comandos dentro da pasta `display`:


- Para construir um App desktop com Electron.js: __[RECOMENDADO]__

```
npm run dev-start
```

- Para rodar pelo browser: (algumas funcionalidades não serão implementadas da maneira correta)

```
npm start
```

## Teste

Para executar os testes unitários basta entrar na pasta `display` e digitar o comando

```
npm run test
```

Saiba mais acessando o arquivo `TESTING.MD` desse repositório.

## Autores

Eric Mendes - @Eric_Mendes

Gabriel de Alcantara Bomfim Silveira - @gabomfim99

Yuri Alexandre Sato - @SatouYuri

## Progresso do desenvolvimento

Registramos um compilado das entregas a cada sprint

- [Sprint 1](https://gitlab.com/mc426-grupo3/rpgmaster/-/milestones/1)
- [Sprint 2](https://www.youtube.com/watch?v=oC28saH4MK4&feature=youtu.be)

