# Estratégia de teste **[Unitário; Funcionalidade; Funcional ou Caixa Preta]**

Para testar nossa aplicação vamos implementar testes unitários de funcionalidade utilizando caixa preta.

## Unitário

Vamos utilizar testes unitários porque é um dos únicos tipos que podemos implementar nessa fase do projeto. Também para garantir que possamos realizar testes de integração, e assim por diante, precisamos primeiro realizar os testes unitários.

## Funcionalidade

Ainda não é viável implementar outros tipos de teste. Para a fase atual do projeto, realizar testes de funcionalidade são suficientes para detectar erros nas implementações e garantir que os critérios das nossas histórias de usuário estão sendo atingidos.

## Funcional ou Caixa Preta

Vamos utilizar caixa preta, porque estes testes independem de implementação. Os testes já implementados podem se manter sempre os mesmos, o que nos poupa tempo e esforço.